# Changelog

### 0.0.2

* [breaking] added serializable/deserializable enums `Priority` and `Status` for use in struct `Task`
* [breaking] renamed function: `get_tasks -> get_all_tasks`
* implemented `ToSql` and `FromSql` for enums `Priority` and `Status`
* set `starttime`, `endtime` and `tags` in the database as optional (not `NOT NULL`)
* added function: `get_tasks_by_ids`
* set functions `tag_str_to_vec` and `tag_vec_to_str` as public
* implemented `std::fmt::Display` for enum `Status`
* implemented `std::str::FromStr` for enum `Priority`
  * introduce Error type `PriorityParseError`
  * added unit tests for `Priority`
* moved `tag_str_to_vec` and `tag_vec_to_str` into module `utils`
  * added unit tests for these functions

### 0.0.3

* [breaking] renamed `starttime` to `startdatetime` and `endtime` to `enddatetime`
* [breaking] switch `startdatetime` and `enddatetime` to using `chrono::offset::FixedOffset`
* [breaking] made `add_task` and `get_tasks_by_ids` borrow params instead of taking ownership
* added utility function `csv_intrange_to_vec`
* `add_task` now returns Ok(id) of the newly created task
* added `get_tasks` for limited and offseted querying
* added `get_total` for getting total number of tasks

### 0.0.4

* [breaking] added new column: `folder`
* added various `UPDATE` functions
* added `get_tasks_by_folder`
