# enpitsu-lib

Rust Library for Enpitsu. Uses SQLite for storing tasks.

---

# Status

Various functionality has been added. See [`CHANGELOG.md`](./CHANGELOG.md)
for various features.
