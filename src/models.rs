use chrono::prelude::*;
use serde::{Serialize, Deserialize};
use rusqlite::types::{
    ToSql, ToSqlOutput,
    FromSql, FromSqlResult, FromSqlError, ValueRef
};
use std::error::Error as StdError;

/// Priority of a Task
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum Priority {
    NoPriority, ImportantAndUrgent, Urgent, Important
}

impl ToSql for Priority {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        let priority = match self {
            Priority::NoPriority => "No priority",
            Priority::ImportantAndUrgent => "Important & Urgent",
            Priority::Urgent => "Urgent",
            Priority::Important => "Important"
        };

        Ok(ToSqlOutput::from(priority))
    }
}

impl FromSql for Priority {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        value.as_str().and_then(|s| match s {
            "No priority" => Ok(Priority::NoPriority),
            "Important & Urgent" => Ok(Priority::ImportantAndUrgent),
            "Urgent" => Ok(Priority::Urgent),
            "Important" => Ok(Priority::Important),
            &_ => Err(FromSqlError::Other(Box::from("Invalid value for enum Priority")))
        })
    }
}

/// Error type for when a string cannot be parsed into a Priority
#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum PriorityParseError {
    /// Unrecognized input string
    Invalid
}

impl StdError for PriorityParseError {
    fn description(&self) -> &str {
        match self {
            PriorityParseError::Invalid => "unrecognized input string"
        }
    }
}

impl std::fmt::Display for PriorityParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.description().fmt(f)
    }
}

impl std::str::FromStr for Priority {
    type Err = PriorityParseError;

    fn from_str(s: &str) -> Result<Priority, PriorityParseError> {
        let important: String = String::from("important");
        let urgent: String = String::from("urgent");
        let urgent_and_important: String = String::from("urgent and important");
        let important_and_urgent: String = String::from("important and urgent");
        let no_priority: String = String::from("no priority");
        let none: String = String::from("none");

        let s_low: String = s.to_lowercase();
        if s_low == important {
            Ok(Priority::Important)
        } else if s_low == urgent {
            Ok(Priority::Urgent)
        } else if s_low == urgent_and_important || s_low == important_and_urgent {
            Ok(Priority::ImportantAndUrgent)
        } else if s_low == no_priority || s_low == none {
            Ok(Priority::NoPriority)
        } else {
            Err(PriorityParseError::Invalid)
        }
    }
}

/// Status of a Task
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum Status {
    Active, Inactive, Completed
}

impl ToSql for Status {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        let status = match self {
            Status::Active => "Active",
            Status::Inactive => "Inactive",
            Status::Completed => "Completed"
        };

        Ok(ToSqlOutput::from(status))
    }
}

impl FromSql for Status {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        value.as_str().and_then(|s| match s {
            "Active" => Ok(Status::Active),
            "Inactive" => Ok(Status::Inactive),
            "Completed" => Ok(Status::Completed),
            &_ => Err(FromSqlError::Other(Box::from("Invalid value for enum Status")))
        })
    }
}

impl std::fmt::Display for Status {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let st = match self {
            Status::Active => "Active",
            Status::Inactive => "Inactive",
            Status::Completed => "Completed"
        };

        write!(f, "{}", st)
    }
}

/// Structure for a Task in Enpitsu
#[derive(Serialize, Deserialize, Debug)]
pub struct Task {
    /// Id of the Task
    pub id: isize,
    /// Title of the Task
    pub title: String,
    /// Beginning of the Task
    ///
    /// `None`: Untimed task  
    /// `Ok(_)`: Timed task
    pub startdatetime: Option<DateTime<FixedOffset>>,
    /// Ending of the Task
    ///
    /// `None`: Untimed task
    /// `Ok(_)`: Timed task
    pub enddatetime: Option<DateTime<FixedOffset>>,
    /// Folder that the Task belongs to
    pub folder: String,
    /// Tags of the task
    pub tags: Vec<String>,
    /// Priority of the Task
    ///
    /// See also `models::Priority`
    pub priority: Priority,
    /// Notes for the Task
    pub notes: String,
    /// Status of the Task
    ///
    /// See also `models::Status`
    pub status: Status,
    /// Is the Task starred?
    pub star: bool,
    /// Creation date of the Task
    pub creation: DateTime<FixedOffset>
}
