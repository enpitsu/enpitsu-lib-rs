use std::error::Error;
use chrono::prelude::*;
use rusqlite::Row;
use crate::models::Task;

/// Format a comma separated string into `Vec<String>`
///
/// Useful for retrieving tags from the database into
/// a type acceptable by `struct Task`
///
/// Example:
/// ```
/// use enpitsu_lib::utils;
/// let s = "tag1,tag2,tag 3".to_owned();
/// assert_eq!{utils::tag_str_to_vec(s), vec!["tag1", "tag2", "tag 3"]};
/// ```
pub fn tag_str_to_vec(string: String) -> Vec<String> {
    string.split(",").map(|s| {
        s.to_string()
    }).collect()
}

/// Format `Vec<String>` into a comma separated string
///
/// This is useful for storing tags into sqlite,
/// since sqlite does not allow for arrays
///
/// Example:
/// ```
/// use enpitsu_lib::utils;
/// let v = vec![String::from("tag1"), String::from("tag2"), String::from(" tag 3")];
/// assert_eq!{utils::tag_vec_to_str(v), "tag1,tag2,tag 3".to_owned()};
/// ```
pub fn tag_vec_to_str(v: Vec<String>) -> String {
    let mut tags = String::from("");
    for tag in v {
        if tags.len() > 1 {
            tags.push(',');
        }
        tags.push_str(&tag.trim());
    }
    tags
}

/// Format a comma-separated string of integers and ranges into a Vec that contains
/// all the covered integers.
///
/// The function will ignore any non-integer values and not add them to the Vec.
///
/// Example:
/// ```
/// use enpitsu_lib::utils::csv_intrange_to_vec;
/// assert_eq!{csv_intrange_to_vec("1,3-5,7".to_string()), [1, 3, 4, 5, 7]};
/// assert_eq!{csv_intrange_to_vec("1,5-3,7".to_string()), [1, 7]};
/// // "a" is not added to the Vec
/// assert_eq!{csv_intrange_to_vec("a,3-5,7".to_string()), [3, 4, 5, 7]};
/// // "-1" is not added to the Vec
/// assert_eq!{csv_intrange_to_vec("-1,3-5,7".to_string()), [3, 4, 5, 7]};
/// ```
pub fn csv_intrange_to_vec(s: String) -> Vec<isize> {
    let raw_targets: Vec<&str> = s.split(',').collect();
    let mut targets: Vec<isize> = Vec::new();
    for raw_target in raw_targets {
        match raw_target.find('-') {
            Some(_n) => {
                match range_to_vec(raw_target) {
                    Ok(e) => targets.extend(e),
                    Err(err) => {
                        println!("enpitsu-lib Warning: {}", err);
                        ()
                    }
                }
            },
            None => {
                match raw_target.parse::<isize>() {
                    Ok(i) => targets.push(i),
                    Err(_e) => ()
                }
            }
        }
    }

    targets
}

/// Convert the string x-y into a Vec of integers ranging from x to y (inclusive of both)
fn range_to_vec(s: &str) -> Result<Vec<isize>, Box<dyn Error>> {
    let startend_str: Vec<&str> = s.split('-').collect();
    let mut startend: Vec<isize> = Vec::with_capacity(2);
    match startend_str[0].parse::<isize>() {
        Ok(i) => startend.push(i),
        Err(err) => {
            println!("enpitsu-lib Warning: {}", err);
            ()
        }
    };
    match startend_str[1].parse::<isize>() {
        Ok(i) => startend.push(i),
        Err(err) => {
            println!("enpitsu-lib Warning: {}", err);
            ()
        }
    }
    let mut values: Vec<isize> = Vec::new();
    if startend.len() == 2 {
        for i in startend[0]..=startend[1] {
            values.push(i);
        }
    }

    Ok(values)
}

/// Convert a row from rusqlite to a Task struct
pub fn row_to_task(row: &Row) -> Task {
    let str_tags = row.get(5).expect("Could not parse row.get(5)");
    let tags = if str_tags == "" { Vec::new() } else { tag_str_to_vec(str_tags) };

    let raw_startdatetime: String = row.get(2).expect("Could not parse row.get(2)");
    let startdatetime = if raw_startdatetime.is_empty() {
        None } else {
        match DateTime::parse_from_rfc3339(&raw_startdatetime) {
            Ok(s) => Some(s),
            Err(err) => {
                println!("enpitsu-lib Error: {}", err);

                None
            }
        }
    };

    let raw_enddatetime: String = row.get(3).expect("Could not parse row.get(3)");
    let enddatetime = if raw_enddatetime.is_empty() {
        None
    } else {
        match DateTime::parse_from_rfc3339(&raw_enddatetime) {
            Ok(s) => Some(s),
            Err(err) => {
                println!("enpitsu-lib Error: {}", err);

                None
            }
        }
    };

    let raw_creation: String = row.get(10).expect("Could not parse row.get(10)");
    let creation = match DateTime::parse_from_rfc3339(&raw_creation) {
        Ok(s) => s,
        Err(err) => {
            println!("enpitsu-lib Error: {}", err);

            Utc.ymd(2020, 02, 07).and_hms(20, 40, 10)
                .with_timezone(&FixedOffset::east(8*3600))
        }
    };

    Task {
        id: row.get(0).expect("Could not parse row.get(0)"),
        title: row.get(1).expect("Could not parse row.get(1)"),
        startdatetime,
        enddatetime,
        folder: row.get(4).expect("Could not parse row.get(4)"),
        tags,
        priority: row.get(6).expect("Could not parse row.get(6)"),
        notes: row.get(7).expect("Could not parse row.get(7)"),
        status: row.get(8).expect("Could not parse row.get(8)"),
        star: row.get(9).expect("Could not parse row.get(9)"),
        creation
    }
}
