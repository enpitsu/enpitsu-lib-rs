pub mod models;
pub mod utils;
#[cfg(test)] mod test;

extern crate chrono;
extern crate serde;

use chrono::prelude::*;
use rusqlite::{params, Connection, Result};
use self::models::*;
use std::path;

/// Enpitsu database struct that acts as a wrapper for
/// [`rusqlite::Connection`](https://docs.rs/rusqlite/0.21.0/rusqlite/struct.Connection.html),
/// along with a path to the database file.
pub struct EnpDB {
    /// Path of the database file
    pub db_path: path::PathBuf,
    conn: Connection
}

impl EnpDB {
    /// Create a new `EnpDB` instance
    pub fn new(db_path: path::PathBuf) -> EnpDB {
        let conn = Connection::open(&db_path).expect("Database not found");
        conn.execute(
            "CREATE TABLE IF NOT EXISTS task (
                id              INTEGER PRIMARY KEY,
                title           TEXT NOT NULL,
                startdatetime   TEXT,
                enddatetime     TEXT,
                folder          TEXT NOT NULL,
                tags            TEXT,
                priority        TEXT NOT NULL,
                notes           TEXT,
                status          TEXT NOT NULL,
                star            INTEGER NOT NULL,
                creation        TEXT NOT NULL
            )",
            params![]
        ).expect("Could not write to db_path");

        EnpDB { db_path, conn }
    }

    /// Add a task to the database
    pub fn add_task(&self, task: &Task) -> Result<isize> {
        let tags = utils::tag_vec_to_str(task.tags.clone());

        self.conn.execute(
            "INSERT INTO task (
                title, startdatetime, enddatetime, folder, tags,
                priority, notes, status, star, creation)
             VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10)",
            params![
                task.title, task.startdatetime, task.enddatetime, task.folder, tags,
                task.priority, task.notes, task.status, task.star, task.creation
            ]
            )?;

        let mut query_result = self.conn.prepare(
            "SELECT id FROM task WHERE title = ?1 AND creation = ?2"
            )?;
        let id = query_result.query_row(params![task.title, task.creation], |row| {
            row.get(0)
        })?;

        Ok(id)
    }

    /// Remove a task from the database by id
    pub fn remove_task(&self, id: isize) -> Result<()> {
        self.conn.execute(
            "DELETE FROM task WHERE id=?1",
            params![id]
        )?;

        Ok(())
    }

    /// Get a limited number tasks with an offset
    /// Defaults:
    ///   - Limit: 50
    ///   - Offset: 0
    pub fn get_tasks(&self, limit: Option<isize>, offset: Option<isize>) -> Result<Vec<Task>> {
        let mut query_result = self.conn.prepare("SELECT * FROM task LIMIT ?1 OFFSET ?2")?;
        let task_iterable = query_result.query_map(
            params![
                match limit {
                    Some(l) => l,
                    None => 50
                },
                match offset {
                    Some(o) => o,
                    None => 0
                }
            ],
            |row| Ok(utils::row_to_task(row)))?;

        let mut v: Vec<Task> = Vec::new();
        for task in task_iterable {
            v.push(task?);
        };

        Ok(v)
    }

    /// Get all tasks from the database
    /// Avoid using this function for large databases
    pub fn get_all_tasks(&self) -> Result<Vec<Task>> {
        let mut query_result = self.conn.prepare("SELECT * FROM task")?;
        let task_iterable = query_result.query_map(params![], |row| Ok(utils::row_to_task(row)))?;

        let mut v: Vec<Task> = Vec::new();
        for task in task_iterable {
            v.push(task?);
        };

        Ok(v)
    }

    /// Query tasks from database by ids
    pub fn get_tasks_by_ids(&self, ids: &Vec<isize>) -> Result<Vec<Task>> {
        let mut tasks: Vec<Task> = Vec::new();
        for id in ids {
            match self.conn.query_row(
                "SELECT * FROM task WHERE id=?1",
                params![id],
                |row| Ok(utils::row_to_task(row))
            ) {
                Ok(t) => tasks.push(t),
                Err(_error) => ()
            };
        };

        Ok(tasks)
    }

    /// Get tasks by folder
    pub fn get_tasks_by_folder(&self, folder: String) -> Result<Vec<Task>> {
        let mut query_result = self.conn.prepare("SELECT * FROM task WHERE folder=?1")?;
        let task_iterable = query_result.query_map(
            params![folder],
            |row| Ok(utils::row_to_task(row))
            )?;

        let mut v: Vec<Task> = Vec::new();
        for task in task_iterable {
            v.push(task?);
        };

        Ok(v)
    }

    /// Get total rows in database
    pub fn get_total(&self) -> Result<isize> {
        let mut query_result = self.conn.prepare(
            "SELECT Count(1) FROM task"
            )?;
        let total = query_result.query_row(params![], |row| {
            row.get(0)
        })?;

        Ok(total)
    }

    /// Update database entry by providing a full task
    pub fn update_task(&self, id: isize, task: &Task) -> Result<Vec<Task>> {
        let tags = utils::tag_vec_to_str(task.tags.clone());
        self.conn.execute(
            "UPDATE task
             SET title=?1, startdatetime=?2, enddatetime=?3,
                 tags=?4, priority=?5, notes=?6, status=?7,
                 star=?8
             WHERE id=?9",
            params![task.title, task.startdatetime, task.enddatetime, tags,
                    task.priority, task.notes, task.status, task.star, id]
            ).expect("Updating task was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update title of database entry
    pub fn update_task_title(&self, id: isize, title: String) -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET title=?1 WHERE id=?2",
            params![title, id]
            ).expect("Updating task title was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update startdatetime of database entry
    pub fn update_task_startdatetime(&self, id: isize, startdatetime: DateTime<FixedOffset>)
        -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET startdatetime=?1 WHERE id=?2",
            params![startdatetime, id]
            ).expect("Updating task startdatetime was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }
    
    /// Update enddatetime of database entry
    pub fn update_task_enddatetime(&self, id: isize, enddatetime: DateTime<FixedOffset>)
        -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET enddatetime=?1 WHERE id=?2",
            params![enddatetime, id]
            ).expect("Updating task enddatetime was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update folder of Task in database
    pub fn update_task_folder(&self, id: isize, folder: &String) -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET folder=?1 WHERE id=?2",
            params![folder, id]
            ).expect("Updating task folder was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update tags of Task in database
    pub fn update_task_tags(&self, id: isize, tags: &Vec<String>) -> Result<Vec<Task>> {
        let stringified_tags = utils::tag_vec_to_str(tags.to_vec());
        self.conn.execute(
            "UPDATE task SET tags=?1 WHERE id=?2",
            params![stringified_tags, id]
            ).expect("Updating task tags was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update priority of Task in database
    pub fn update_task_priority(&self, id: isize, priority: Priority)
        -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET priority=?1 WHERE id=?2",
            params![priority, id]
            ).expect("Updating task priority was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update notes of Task in database
    pub fn update_task_notes(&self, id: isize, notes: String) -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET notes=?1 WHERE id=?2",
            params![notes, id]
            ).expect("Updating task notes was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update status of Task in database
    pub fn update_task_status(&self, id: isize, status: Status) -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET status=?1 WHERE id=?2",
            params![status, id]
            ).expect("Updating task status was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update star of Task in database
    pub fn update_task_star(&self, id: isize, star: bool) -> Result<Vec<Task>> {
        self.conn.execute(
            "UPDATE task SET star=?1 WHERE id=?2",
            params![star, id]
            ).expect("Updating task star was unsuccessful");

        self.get_tasks_by_ids(&vec![id])
    }

    /// Update folder name
    pub fn update_folder_name(&self, old_name: String, new_name: String)
        -> Result<()> {
        self.conn.execute(
            "UPDATE task SET folder=?1 WHERE folder=?2",
            params![new_name, old_name]
            ).expect("Could not update folder name");

        Ok(())
    }
}
