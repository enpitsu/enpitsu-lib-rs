use chrono::prelude::*;
use std::{ fs, str::FromStr, thread, time };
use super::*;
use super::models::*;

#[test]
fn adding_task() {
    let r = EnpDB::new(path::PathBuf::from("./add.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0, // This is a dummy value
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    let ten_millis = time::Duration::from_millis(10);
    thread::sleep(ten_millis);

    let s = Task {
        id: 0, // This is a dummy value
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 2".to_string(),
        tags,
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    assert_eq!{r.add_task(&t), Ok(1)};
    assert_eq!{r.add_task(&s), Ok(2)};

    fs::remove_file("./add.db").expect("Could not remove add.db");
}

#[test]
fn removing_task() {
    let r = EnpDB::new(path::PathBuf::from("./remove.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags,
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task");
    assert_eq!{r.remove_task(1), Ok(())};

    fs::remove_file("./remove.db").expect("Could not remove remove.db");
}

#[test]
fn getting_tasks() {
    let r = EnpDB::new(path::PathBuf::from("./get.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task");
    let tasks = r.get_all_tasks().expect("Could not get tasks");
    assert_eq!{tasks[0].title, "Task 1"};
    assert_eq!{tasks[0].startdatetime, Some(starttime)};
    assert_eq!{tasks[0].enddatetime, Some(endtime)};
    assert_eq!{tasks[0].folder, "Folder 1"};
    assert_eq!{tasks[0].tags, tags};
    assert_eq!{tasks[0].priority, Priority::NoPriority};
    assert_eq!{tasks[0].notes, String::from("This is some note")};
    assert_eq!{tasks[0].status, Status::Active};
    assert_eq!{tasks[0].star, true};

    fs::remove_file("./get.db").expect("Could not remove get.db");
}

#[test]
fn edge_case_empty_tag() {
    let r = EnpDB::new(path::PathBuf::from("./empty_tag.db"));

    let tags = Vec::new();
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags,
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task");
    let tasks = r.get_all_tasks().expect("Could not get tasks");
    let empty_vec: Vec<String> = Vec::new();
    assert_eq!{tasks[0].tags, empty_vec};

    fs::remove_file("./empty_tag.db").expect("Could not remove empty_tag.db");
}

#[test]
fn get_task_by_ids() {
    let r = EnpDB::new(path::PathBuf::from("./get_by_ids.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };
    let s = Task {
        id: 1,
        title: String::from("Task 2"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 2".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task t");
    r.add_task(&s).expect("Could not add task s");

    let mut tasks = r.get_tasks_by_ids(&vec![1]).expect("Could not get tasks");
    assert_eq!{tasks[0].title, "Task 1"};
    tasks = r.get_tasks_by_ids(&vec![2]).expect("Could not get task");
    assert_eq!{tasks[0].title, "Task 2"};

    fs::remove_file("./get_by_ids.db").expect("Could not remove get_by_ids.db");
}

#[test]
fn test_priority_from_str() {
    assert_eq!{Priority::from_str("no priority"), Ok(Priority::NoPriority)};
    assert_eq!{Priority::from_str("No Priority"), Ok(Priority::NoPriority)};
    assert_eq!{Priority::from_str("important"), Ok(Priority::Important)};
    assert_eq!{Priority::from_str("Important"), Ok(Priority::Important)};
    assert_eq!{Priority::from_str("urgent"), Ok(Priority::Urgent)};
    assert_eq!{Priority::from_str("urGent"), Ok(Priority::Urgent)};
    assert_eq!{Priority::from_str("important and urgent"), Ok(Priority::ImportantAndUrgent)};
    assert_eq!{Priority::from_str("urgent and Important"), Ok(Priority::ImportantAndUrgent)};
    assert_eq!{Priority::from_str("gibberish"), Err(PriorityParseError::Invalid)};
}

#[test]
fn get_tasks_with_limit_and_offset() {
    let r = EnpDB::new(path::PathBuf::from("./get_tasks_with_limit_and_offset.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };
    let s = Task {
        id: 1,
        title: String::from("Task 2"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 2".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task t");
    r.add_task(&s).expect("Could not add task s");

    let tasks = r.get_tasks(Some(1), Some(1)).expect("Could not get tasks");
    assert_eq!{tasks[0].title, "Task 2"};

    fs::remove_file("./get_tasks_with_limit_and_offset.db")
        .expect("Could not remove get_tasks_with_limit_and_offset.db");
}

#[test]
fn count_rows() {
    let r = EnpDB::new(path::PathBuf::from("./count_rows.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };
    let s = Task {
        id: 1,
        title: String::from("Task 2"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 2".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task t");
    r.add_task(&s).expect("Could not add task s");

    let total = r.get_total();
    assert_eq!{total, Ok(2)};

    fs::remove_file("./count_rows.db").expect("Could not remove count_rows.db");
}

#[test]
fn update_task() {
    let r = EnpDB::new(path::PathBuf::from("./update.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0,
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };
    r.add_task(&t).expect("Could not add task t");

    let s = Task {
        id: 1,
        title: String::from("Task 2"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 2".to_string(),
        tags: tags.clone(),
        priority: Priority::Urgent,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    let tasks = r.update_task(1, &s).expect("Could not update task");
    assert_eq!{tasks[0].title, "Task 2"};
    assert_eq!{tasks[0].priority, Priority::Urgent};
    r.update_task(2, &s).expect("Could not update nonexistent task 2");

    // Test title update
    let update_title = r.update_task_title(1, "Task 3".to_string()).expect("Could not update title");
    assert_eq!{update_title[0].title, "Task 3"};

    // Test start date time update
    let new_startdatetime = Local.ymd(2020, 3, 2).and_hms(19, 39, 39)
        .with_timezone(&FixedOffset::east(8*3600));
    let update_startdatetime = r.update_task_startdatetime(1, new_startdatetime)
        .expect("Could not update startdatetime");
    assert_eq!{update_startdatetime[0].startdatetime, Some(new_startdatetime)};

    // Test end date time update
    let new_enddatetime = Local.ymd(2020, 3, 2).and_hms(19, 39, 39)
        .with_timezone(&FixedOffset::east(8*3600));
    let update_startdatetime = r.update_task_startdatetime(1, new_enddatetime)
        .expect("Could not update enddatetime");
    assert_eq!{update_startdatetime[0].startdatetime, Some(new_enddatetime)};

    // Test update folder name
    let update_folder = r.update_task_folder(1, &"Folder X".to_string())
        .expect("Could not udpate folder");
    assert_eq!{update_folder[0].folder, "Folder X"};

    // Test update tags
    let new_tags = vec!["t1".to_string(), "t2".to_string()];
    let update_tags = r.update_task_tags(1, &new_tags)
        .expect("Could not update tags");
    assert_eq!{update_tags[0].tags, new_tags};

    // Test update priority
    let update_priority = r.update_task_priority(1, Priority::Important)
        .expect("Could not update priority");
    assert_eq!{update_priority[0].priority, Priority::Important};

    // Test update notes
    let update_notes = r.update_task_notes(1, "This note is updated".to_string())
        .expect("Could not update notes");
    assert_eq!{update_notes[0].notes, "This note is updated"};

    // Test update status
    let update_status = r.update_task_status(1, Status::Inactive)
        .expect("Could not update status");
    assert_eq!{update_status[0].status, Status::Inactive};

    // Test update star
    let update_star = r.update_task_star(1, false).expect("Could not update star");
    assert_eq!{update_star[0].star, false};

    fs::remove_file("./update.db").expect("Could not remove update.db");
}

#[test]
fn get_tasks_by_folder() {
    let r = EnpDB::new(path::PathBuf::from("./get_tasks_by_folder.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0, // This is a dummy value
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    let ten_millis = time::Duration::from_millis(10);
    thread::sleep(ten_millis);

    let s = Task {
        id: 0, // This is a dummy value
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 2".to_string(),
        tags,
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };

    r.add_task(&t).expect("Could not add task t");
    r.add_task(&s).expect("Could not add task s");

    let tasks = r.get_tasks_by_folder("Folder 1".to_string())
        .expect("Could not get task by folder");
    assert_eq!{tasks.len(), 1};
    assert_eq!{tasks[0].folder, "Folder 1"};

    let no_tasks = r.get_tasks_by_folder("Not a folder".to_string())
        .expect("Could not get task by folder");
    assert_eq!{no_tasks.len(), 0};

    fs::remove_file("./get_tasks_by_folder.db")
        .expect("Could not remove get_tasks_by_folder.db");
}

#[test]
fn rename_folder() {
    let r = EnpDB::new(path::PathBuf::from("./rename_folder.db"));

    let tags = vec![String::from("tag1"), String::from("tag2")];
    let starttime = Local.ymd(2020, 1, 8).and_hms(13, 49, 57)
        .with_timezone(&FixedOffset::east(8*3600));
    let endtime = Local.ymd(2020, 1, 9).and_hms(12, 0, 0)
        .with_timezone(&FixedOffset::east(8*3600));
    let t = Task {
        id: 0, // This is a dummy value
        title: String::from("Task 1"),
        startdatetime: serde::export::Some(starttime),
        enddatetime: serde::export::Some(endtime),
        folder: "Folder 1".to_string(),
        tags: tags.clone(),
        priority: Priority::NoPriority,
        notes: String::from("This is some note"),
        status: Status::Active,
        star: true,
        creation: Local::now().with_timezone(&FixedOffset::east(8*3600))
    };
    r.add_task(&t).expect("Could not add task t");
    
    assert_eq!{
        r.update_folder_name("Folder 1".to_string(), "New Folder".to_string()),
        Ok(())
    };

    let tasks = r.get_tasks_by_folder("New Folder".to_string())
        .expect("Could not get task by folder");
    assert_eq!{tasks.len(), 1};

    fs::remove_file("./rename_folder.db")
        .expect("Could not remove rename_folder.db");
}
